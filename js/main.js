// solutions silder js below //
// $(function() {
//     var oTop = $('.bg-map').offset().top - window.innerHeight;
//     $(window).scroll(function(){

//         var pTop = $('body').scrollTop();
//         console.log( pTop + ' - ' + oTop );   //just for your debugging
//         if( pTop > oTop ){
            
//         }
//     });
// });
$(document).ready( function() {
    $('#myCarousel').carousel({      
        interval:   4000
  });

    $(".solutions-tabs li a").click(function(){
      if($(this).text().trim()=="Institutional Equities")
        currentSlide1(slideIndex1);
      else
        currentSlide(slideIndex);
    });
  
  var clickEvent = false;
  $('#myCarousel').on('click', '.nav-value a', function() {
      clickEvent = true;
      $('.nav-value li').removeClass('active');
      $(this).parent().addClass('active');    
  }).on('slid.bs.carousel', function(e) {
    if(!clickEvent) {
      var count = $('.nav-value').children().length -1;
      var current = $('.nav-value li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if(count == id) {
        $('.nav-value li').first().addClass('active');  
      }
    }
    clickEvent = false;
  });
});
// solutions silder js below //

// solutions silder js below //
function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function currentSlide1(n) {
  showSlides1(n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  if(dots.length>0)
    dots[slideIndex-1].className += " active";
}
// solutions silder js above //

// solutions silder js below //
function plusSlides1(n) {
  showSlides1(slideIndex1 += n);
}

function showSlides1(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides1");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex1 = 1}    
  if (n < 1) {slideIndex1 = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex1-1].style.display = "block";
  if(dots.length>0)
    dots[slideIndex1-1].className += " active";
}
// solutions silder js above //



// Smooth page scroll below //
$(".navbar-nav a").click(function(event) {
  //event.preventDefault(); // get hash value
  var attrHref = $(this).attr('href');
  $('html,body').animate({
    scrollTop: $(attrHref).offset().top},
    2000);
});

    // $('.noAction').on('click', function () {
    //   location.reload();
    // })
// Smooth page scroll above //

// Header navigation active below //
$('.sun-menu ul.nav li a, .menu .navbar-nav li a').click(function () {
  var $this = $(this);
  $this.siblings().removeClass('active');
  $this.addClass('active');
});
// Header navigation active above //

// Counter silder js below //
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  arrows:false,
  centerMode: true,
  speed: 1000,
  centerPadding: '0px',
  focusOnSelect: true,
  vertical: true,
  verticalSwiping: true,
  autoplay: false
});
  // Counter silder detalis js below //
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: false,
    asNavFor: '.slider-nav',
    dots: false,
    arrows:true,  
    vertical: true,
    verticalSwiping: true,
    appendArrows: '.pr_images',
    speed: 1000,
    //prevArrow:'<i class="fa fa-angle-left slick-prev"></i>',
    //nextArrow:'<i class="fa fa-angle-right slick-next"></i>'
  });
// Counter silder js above //

// Story silder js below //
$('.slider-for-story').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav-story',
  autoplay: false
});
   // Story silder logo icon js below //
   $('.slider-nav-story').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for-story',
    dots: true,
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    variableWidth: true
  });
// Story silder js above  //

// Media center js below  //

$('.media-slider').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true,
  autoplay: false
});

// Media center js above  //

// Video slider js below - Media center page  //

$('.Video-slider').slick({
  dots: true,
  infinite: true,
  speed: 1000,
  slidesToShow: 1,
  adaptiveHeight: true,
  autoplay: false
});

// Video slider js above - Media center page  //

// solutions silder js below //
var slideIndex1 = 1;
$(document).ready(function() {
var slideIndex = 1;

//showSlides(slideIndex);
currentSlide(1);
});
// solutions silder js above //

// Equity page - Brand area below //
$(".bg-layer").on('mouseover', function(e){
  $(this).find("img").css({"-webkit-filter": "grayscale(0%)",
    "-o-filter": "grayscale(0%)",
    "-moz-filter": "grayscale(0%)",
    "filter": "grayscale(0%)"});
});
$(".bg-layer").on('mouseout', function(e){
  $(this).find("img").css({"-webkit-filter": "grayscale(100%)",
    "-o-filter": "grayscale(100%)",
    "-moz-filter": "grayscale(100%)",
    "filter": "grayscale(100%)"});
});
// Equity page - Brand area above //

// Career page our team - js below //
$(".team-bg .img-container").on('mouseover', function(e){
  $(this).find("img").css({"-webkit-filter": "brightness(100%)",
    "-o-filter": "brightness(100%)",
    "-moz-filter": "brightness(100%)",
    "filter": "brightness(100%)",
    "border": "10px solid #ae275f"
    });
  $(this).find(".text-para").css({
    "color": "#fff"
  });
});

$(".team-bg .img-container").on('mouseout', function(e){
  $(this).find("img").css({"-webkit-filter": "brightness(0.30)",
    "-o-filter": "brightness(0.30)",
    "-moz-filter": "brightness(0.30)",
    "filter": "brightness(0.30)",
    "border": "0px"
  });
  $(this).find(".text-para").css({
    "color": "#9c9a9a"
  });
});
// Career page our team - js above //


// Counter Count area & Map Section js below
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 10000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
// Counter Count area & Map Section js above




//Career page Opening arrow js below

$(".panel-default").click(function(event) {
  var open_id = $(this).find(".panel-collapse").attr('id');
  $(".panel-title").each(function(){
    console.log("in")
    $(this).find('a').removeClass("act");
    // alert("#"+open_id)
     $(this).find('a').html('Lorem Ipsum');

  });
  console.log(open_id)
  setTimeout(function(){ 
    if($("#"+open_id).hasClass("in")){
      $("#"+open_id).parent().find('a').addClass("act");
       if($("#"+open_id).parent().find('a').hasClass("act")){
        $("#"+open_id).parent().find('a').append('<span class="icon-Right-arrow"></span>');
       }
    }        
  }, 100);
});

//Career page Opening arrow js above

// Mobile Responsive Menu
$(".toggle-nav").click(function() {
    return $("#nav").toggleClass("nav-open"), $("body").toggleClass("scroll"), $("html, body").scrollTop(0), !1
}),
$("#sub-menu > a").click(function(e) {
    var t = $(this).parent();
    $("ul#nav").hasClass("nav-open") && (e.preventDefault(), $(this).toggleClass("back-link"), $("#nav > li:not()").toggle(), t.toggle().toggleClass("nav-expanded"), $("html, body").scrollTop(0))
})

// Mobile Responsive Menu ICON TOGGLE
$(".toggle-nav").click(function() {
    $(".toggle-nav").toggleClass('toggle-nav-close');
})



